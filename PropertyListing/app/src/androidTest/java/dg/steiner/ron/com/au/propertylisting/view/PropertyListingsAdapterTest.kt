package dg.steiner.ron.com.au.propertylisting.view

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.ViewGroup
import android.widget.FrameLayout
import dagger.Component
import dagger.Module
import dagger.Provides
import dg.steiner.ron.com.au.propertylisting.TestActivity
import dg.steiner.ron.com.au.propertylisting.utils.ImageLoadHelper
import io.mockk.*
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.*
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tests.utils.*

@RunWith(AndroidJUnit4::class)
class PropertyListingsAdapterTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(TestActivity::class.java)

    lateinit var viewGroop: ViewGroup

    // mock two PropertyListingListItem to use in the tests
    var items = Array<PropertyListingListViewModel>(2) { _ -> mockk() }

    lateinit var mocks: Mocks

    // SUT
    lateinit var adapter: PropertyListingsAdapter

    @Before
    fun setUp() {
        val activity = activityRule.activity
        viewGroop = FrameLayout(activity)//.findViewById(R.id.content)
        // set the first item as a premium listing and the second item normal:
        with(items[0]) {
            every { isPremium } returns true
            every { primaryText } returns THE_PRIMARY_TEXT_FOR_PREMIUM_LISTING // from the tests.utils package
            every { secondaryText } returns THE_SECONDARY_TEXT_FOR_PREMIUM_LISTING
            every { tertiaryText } returns THE_TERTIARY_TEXT_FOR_PREMIUM_LISTING
            every { firstImageUrl } returns THE_1ST_IMAGE_FOR_PREMIUM_LISTING
            every { secondImageUrl } returns THE_2ND_IMAGE_FOR_PREMIUM_LISTING
            every { agencyLogoUrl } returns THE_AGENCY_LOGO_URL
            every { comparableDescription } returns "first"
        }
        with(items[1]) {
            every { isPremium } returns false
            every { primaryText } returns THE_PRIMARY_TEXT_FOR_REGULAR_LISTING
            every { secondaryText } returns THE_SECONDARY_TEXT_FOR_REGULAR_LISTING
            every { tertiaryText } returns THE_TERTIARY_TEXT_FOR_REGULAR_LISTING
            every { firstImageUrl } returns THE_IMAGE_FOR_REGULAR_LISTING
            every { secondImageUrl } returns null
            every { agencyLogoUrl } returns THE_AGENCY_LOGO_URL
            every { comparableDescription } returns "second"

        }

        mocks = Mocks()

        adapter = PropertyListingsAdapter()
        DaggerPropertyListingsAdapterTest_TestComponent
            .builder()
            .mocks(mocks)
            .build()
            .inject(adapter)

        adapter.items = items
    }

    @Test
    fun that_adapter_creates_correct_view_holder_for_premium_listing() {

        // a premium listing view should have two image-views

        adapter.onCreateViewHolder(viewGroop, adapter.getItemViewType(0)).let { its ->
            // only the premium value queried for view-holder creation
            with(items[0]) {
                verify { isPremium }
                verify(exactly = 0) { primaryText }
                verify(exactly = 0) { secondaryText }
                verify(exactly = 0) { primaryText }
                verify(exactly = 0) { firstImageUrl }
                verify(exactly = 0) { secondImageUrl }
                verify(exactly = 0) { agencyLogoUrl }
            }

            assertNotNull(its.secondaryImage)
        }
    }

    @Test
    fun that_adapter_creates_correct_view_holder_for_normal_listing() {
        // a normal listing view should find only one image-view
        adapter.onCreateViewHolder(viewGroop, adapter.getItemViewType(1)).let { its ->

            // only the premium value queried for view-holder creation
            with(items[1]) {
                verify { isPremium }
                verify(exactly = 0) { primaryText }
                verify(exactly = 0) { secondaryText }
                verify(exactly = 0) { primaryText }
                verify(exactly = 0) { firstImageUrl }
                verify(exactly = 0) { secondImageUrl }
                verify(exactly = 0) { agencyLogoUrl }
            }
            assertNull(its.secondaryImage)
        }
    }

    @Test
    fun that_adapter_binds_correct_values_to_a_premium_listing_view() {

        adapter.onCreateViewHolder(viewGroop, adapter.getItemViewType(0)).also {

            // only the premium value queried for view-holder creation
            with(items[0]) {
                verify { isPremium }
                verify(exactly = 0) { primaryText }
                verify(exactly = 0) { secondaryText }
                verify(exactly = 0) { primaryText }
                verify(exactly = 0) { firstImageUrl }
                verify(exactly = 0) { secondImageUrl }
                verify(exactly = 0) { agencyLogoUrl }
            }

            adapter.onBindViewHolder(it, 0)

            // assert that the texts were set correctly
            with(it) {
                assertThat(
                    primaryText.text.toString(),
                    `is`(THE_PRIMARY_TEXT_FOR_PREMIUM_LISTING)
                )
                assertThat(
                    secondaryText.text.toString(),
                    `is`(THE_SECONDARY_TEXT_FOR_PREMIUM_LISTING)
                )
                assertThat(
                    tertiaryText.text.toString(),
                    `is`(THE_TERTIARY_TEXT_FOR_PREMIUM_LISTING)
                )

                // verify that the loader tried loading the images for the listing
                with(mocks.imageLoadHelper) {
                    verify { load(THE_1ST_IMAGE_FOR_PREMIUM_LISTING, it.primaryImage) }
                    verify { load(THE_2ND_IMAGE_FOR_PREMIUM_LISTING, it.secondaryImage) }
                    verify { load(THE_AGENCY_LOGO_URL, it.agencyLogoImage) }
                }
            }
        }
        // verify that the item was queried for texts
        with(items[0]) {
            verify { primaryText }
            verify { secondaryText }
            verify { primaryText }
            verify { firstImageUrl }
            verify { secondImageUrl }
            verify { agencyLogoUrl}
        }

    }

    @Test
    fun that_adapter_binds_correct_values_to_a_normal_listing_view() {

        adapter.onCreateViewHolder(viewGroop, adapter.getItemViewType(1)).also {
            // only the premium value queried for view-holder creation
            with(items[1]) {
                verify { isPremium }
                verify(exactly = 0) { primaryText }
                verify(exactly = 0) { secondaryText }
                verify(exactly = 0) { primaryText }
                verify(exactly = 0) { firstImageUrl }
                verify(exactly = 0) { secondImageUrl }
                verify(exactly = 0) { agencyLogoUrl }
            }

            adapter.onBindViewHolder(it, 1)

            // assert that the texts were set correctly
            with(it) {
                assertThat(
                    primaryText.text.toString(),
                    `is`(THE_PRIMARY_TEXT_FOR_REGULAR_LISTING)
                )
                assertThat(
                    secondaryText.text.toString(),
                    `is`(THE_SECONDARY_TEXT_FOR_REGULAR_LISTING)
                )
                assertThat(
                    tertiaryText.text.toString(),
                    `is`(THE_TERTIARY_TEXT_FOR_REGULAR_LISTING)
                )

                // verify that the loader tried loading the images for the listing
                with(mocks.imageLoadHelper) {
                    verify {
                        load(THE_IMAGE_FOR_REGULAR_LISTING, it.primaryImage)
                    }
                    verify {
                        load(null, it.secondaryImage)
                    }
                    verify {
                        load(THE_AGENCY_LOGO_URL, it.agencyLogoImage)
                    }
                }
            }

            // assert that the texts were retrieved from the correct item
            with(items[1]) {
                verify { primaryText }
                verify { secondaryText }
                verify { primaryText }
                verify { firstImageUrl }
                verify { secondImageUrl }
                verify { agencyLogoUrl }
            }
        }
    }

    @Test
    fun that_adapter_binds_empty_values_to_the_views_when_item_not_found() {

        adapter.onCreateViewHolder(viewGroop, adapter.getItemViewType(1)).also {

            adapter.onBindViewHolder(it, 2) // no such item

            with(it) {
                assertTrue(
                    secondaryText.text.isEmpty()
                )
                assertTrue(
                    primaryText.text.isEmpty()
                )
                assertTrue(
                    tertiaryText.text.isEmpty()
                )
            }
        }

        with(items[0]) {
            verify(exactly = 0) { primaryText }
            verify(exactly = 0) { secondaryText }
            verify(exactly = 0) { tertiaryText }
        }

        with(items[1]) {
            verify(exactly = 0) { primaryText }
            verify(exactly = 0) { secondaryText }
            verify(exactly = 0) { tertiaryText }
        }

    }


    /* a component that injects mocks */
    @Component(modules = [Mocks::class])
    interface TestComponent {
        fun inject(adapter: PropertyListingsAdapter)
    }

    /* a module with all the mocks */
    @Module
    class Mocks {

        init {
            MockKAnnotations.init(this)
            every { imageLoadHelper.load(any(), any()) } just Runs
        }

        @MockK
        lateinit var imageLoadHelper: ImageLoadHelper

        @Provides
        fun provideImageLoadHelper(): ImageLoadHelper = imageLoadHelper
    }
}

