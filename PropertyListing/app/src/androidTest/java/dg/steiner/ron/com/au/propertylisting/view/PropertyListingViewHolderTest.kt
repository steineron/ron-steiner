package dg.steiner.ron.com.au.propertylisting.view

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.LayoutInflater
import android.view.View
import dg.steiner.ron.com.au.propertylisting.R
import dg.steiner.ron.com.au.propertylisting.TestActivity
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/*
* test the inflation and binding of the view to the view-holder
*
* */

@RunWith(AndroidJUnit4::class)
class PropertyListingViewHolderTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(TestActivity::class.java)

    private lateinit var premiumView: View
    private lateinit var regularView: View
    private lateinit var premiumViewHolder: PropertyListingViewHolder
    private lateinit var regularViewHolder: PropertyListingViewHolder

    @Before
    fun setUp() {
        val activity = activityRule.activity
        premiumView = LayoutInflater.from(activity)
            .inflate(R.layout.list_item_premium_listing, activity.findViewById(R.id.content))

        regularView = LayoutInflater.from(activity)
            .inflate(R.layout.list_item_regular_listing, activity.findViewById(R.id.content))

        premiumViewHolder = PropertyListingViewHolder(premiumView)
        regularViewHolder = PropertyListingViewHolder(regularView)
    }

    @Test
    fun that_view_holder_finds_and_binds_all_views() {
        with(premiumViewHolder) {
            assertNotNull(primaryImage)
            assertNotNull(secondaryImage)
            assertNotNull(primaryText)
            assertNotNull(secondaryText)
            assertNotNull(tertiaryText)
            assertNotNull(agencyLogoImage)
        }

        with(regularViewHolder) {
            assertNotNull(primaryImage)
            assertNull(secondaryImage)
            assertNotNull(primaryText)
            assertNotNull(secondaryText)
            assertNotNull(tertiaryText)
            assertNotNull(agencyLogoImage)
        }
    }
}