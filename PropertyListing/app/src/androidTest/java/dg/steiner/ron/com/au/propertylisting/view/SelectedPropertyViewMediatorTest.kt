package dg.steiner.ron.com.au.propertylisting.view

import android.support.test.rule.ActivityTestRule
import android.widget.TextView
import dg.steiner.ron.com.au.propertylisting.R
import dg.steiner.ron.com.au.propertylisting.TestActivity
import dg.steiner.ron.com.au.propertylisting.view.mediators.SelectedPropertyViewMediator
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SelectedPropertyViewMediatorTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(TestActivity::class.java)

    lateinit var viewMediator: SelectedPropertyViewMediator
    @Before
    fun setUp() {
        val view = TextView(activityRule.activity)
        view.id = R.id.selected_item
        viewMediator =
                SelectedPropertyViewMediator(view)

    }

    @After
    fun tearDown() {
    }


    @Test
    fun that_mediator_binds_to_view() {
        assertNotNull(viewMediator.text)
    }


}