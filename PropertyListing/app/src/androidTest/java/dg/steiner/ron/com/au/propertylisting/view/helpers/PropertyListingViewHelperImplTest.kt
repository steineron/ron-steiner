package dg.steiner.ron.com.au.propertylisting.view.helpers

import android.support.test.InstrumentationRegistry
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import dg.steiner.ron.com.au.propertylisting.TestActivity
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingResponse
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tests.utils.TestUtils

@RunWith(AndroidJUnit4::class)
class PropertyListingViewHelperImplTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(TestActivity::class.java)

    @RelaxedMockK
    lateinit var item: ListingItem

    val testUtils = TestUtils()

    //SUT
    lateinit var viewHelper: PropertyListingViewHelperImpl

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        val activity = activityRule.activity
        viewHelper = PropertyListingViewHelperImpl(activity)

        // build a response from captured one
        item = testUtils.objectFromStream(
            InstrumentationRegistry.getContext().resources.openRawResource(
                dg.steiner.ron.com.au.propertylisting.test.R.raw.listing_response
            ),
            ListingResponse::class.java
        ).listingResults?.listings?.get(0)!!

    }

    @Test
    fun that_helper_returns_correct_price_text() {
        assertEquals("Auction", viewHelper.getPriceText(item).toString())
    }

    @Test
    fun that_helper_returns_correct_price_text_when_price_is_null() {
        item.displayPrice = null
        assertEquals("", viewHelper.getPriceText(item).toString())
    }

    @Test
    fun that_helper_returns_correct_specifications_text() {
        assertEquals("6 Beds, 4 Baths, 2 Cars", viewHelper.getSpecificationsText(item).toString())
    }

    @Test
    fun that_helper_returns_correct_specifications_text_when_bathrooms_is_one() {
        item.bathrooms = 1
        assertEquals("6 Beds, 1 Bath, 2 Cars", viewHelper.getSpecificationsText(item).toString())
    }

    @Test
    fun that_helper_returns_correct_specifications_text_when_bathrooms_is_null() {
        item.bathrooms = null
        assertEquals("6 Beds, 2 Cars", viewHelper.getSpecificationsText(item).toString())

    }

    @Test
    fun that_helper_returns_correct_specifications_text_when_carspacess_is_one() {
        item.carspaces = 1
        assertEquals("6 Beds, 4 Baths, 1 Car", viewHelper.getSpecificationsText(item).toString())
    }

    @Test
    fun that_helper_returns_correct_specifications_text_when_carspaces_is_null() {
        item.carspaces = null
        assertEquals("6 Beds, 4 Baths", viewHelper.getSpecificationsText(item).toString())

    }

    @Test
    fun that_helper_returns_correct_specifications_text_when_bedrooms_is_one() {
        item.bedrooms = 1
        assertEquals("1 Bed, 4 Baths, 2 Cars", viewHelper.getSpecificationsText(item).toString())

    }

    @Test
    fun that_helper_returns_correct_specifications_text_when_bedrooms_is_null() {
        item.bedrooms = null
        assertEquals("4 Baths, 2 Cars", viewHelper.getSpecificationsText(item).toString())

    }


    @Test
    fun that_helper_returns_correct_address_text() {
        assertEquals("79 Anglesea Street, Bondi", viewHelper.getAddressText(item).toString())
    }

    @Test
    fun that_helper_returns_correct_address_text_when_adress_is_null() {
        item.displayableAddress = null
        assertEquals("", viewHelper.getAddressText(item).toString())
    }


    @Test
    fun that_helper_extracts_color_correctly(){
        item.agencyColour = "#0000FF"
        assertEquals(255, viewHelper.getAgencyColor(item))
    }

    @After
    fun tearDown() {
    }
}