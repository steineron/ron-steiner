package dg.steiner.ron.com.au.propertylisting.view

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.annotation.UiThreadTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import dg.steiner.ron.com.au.propertylisting.R
import dg.steiner.ron.com.au.propertylisting.TestActivity
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingResponse
import dg.steiner.ron.com.au.propertylisting.view.mediators.PropertiesListViewMediator
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import junit.framework.Assert.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tests.utils.*

@RunWith(AndroidJUnit4::class)
class PropertiesListViewMediatorTest {
    @Rule
    @JvmField
    val activityRule = LocalTestRule() //dont launch it

    private val testUtils: TestUtils = TestUtils()

    // mock two PropertyListingListItem to use in the tests
    var items = Array<PropertyListingListViewModel>(2) { _ -> mockk() }

    lateinit var listingResponse: ListingResponse

    //SUT
    lateinit var viewMediator: PropertiesListViewMediator

    lateinit var activity: TestActivity

    @MockK
    lateinit var interactions: PropertiesListViewMediator.Interactions

    @Before
    @UiThreadTest
    fun setUp() {

        testUtils.convertAllSchedulersToTest()
        MockKAnnotations.init(this)

        activity = activityRule.activity

        listingResponse = testUtils.objectFromStream(
            InstrumentationRegistry.getContext().resources.openRawResource(
                dg.steiner.ron.com.au.propertylisting.test.R.raw.listing_response
            ),
            ListingResponse::class.java
        )!!

        // set the first item as a premium listing and the second item normal:
        with(items[0]) {
            every { isPremium } returns true
            every { primaryText } returns THE_PRIMARY_TEXT_FOR_PREMIUM_LISTING // from the tests.utils package
            every { secondaryText } returns THE_SECONDARY_TEXT_FOR_PREMIUM_LISTING
            every { tertiaryText } returns THE_TERTIARY_TEXT_FOR_PREMIUM_LISTING
            every { firstImageUrl } returns THE_1ST_IMAGE_FOR_PREMIUM_LISTING
            every { secondImageUrl } returns THE_2ND_IMAGE_FOR_PREMIUM_LISTING
            every { item } returns listingResponse.listingResults?.listings?.get(0)
        }

        with(items[1]) {
            every { isPremium } returns false
            every { primaryText } returns THE_PRIMARY_TEXT_FOR_REGULAR_LISTING
            every { secondaryText } returns THE_SECONDARY_TEXT_FOR_REGULAR_LISTING
            every { tertiaryText } returns THE_TERTIARY_TEXT_FOR_REGULAR_LISTING
            every { firstImageUrl } returns THE_IMAGE_FOR_REGULAR_LISTING
            every { secondImageUrl } returns null
            every { item } returns listingResponse.listingResults?.listings?.get(1)
        }
    }

    @After
    fun tearDown(){
        testUtils.resetSchedulers()
    }

    @Test
    @UiThreadTest
    fun that_mediator_finds_all_views() {
        viewMediator =
                PropertiesListViewMediator(
                    activity.findViewById(R.id.swipe_refresh)
                )

        assertNotNull(viewMediator.swipeToRefresh)
        assertNotNull(viewMediator.recyclerView)
    }

    @Test
    @UiThreadTest
    fun that_mediator_displays_progress_bar_when_refreshing() {
        viewMediator =
                PropertiesListViewMediator(
                    activity.findViewById(R.id.swipe_refresh)
                )

        assertFalse(viewMediator.swipeToRefresh.isRefreshing)

        viewMediator.inProgress = true

        assertTrue(viewMediator.swipeToRefresh.isRefreshing)

    }

}

class LocalTestRule : ActivityTestRule<TestActivity>(TestActivity::class.java) {

    override fun getActivityIntent(): Intent {
        return TestActivity.intentForLayout(
            R.layout.fragment_propertiy_listings,
            InstrumentationRegistry.getTargetContext()
        )
    }
}
