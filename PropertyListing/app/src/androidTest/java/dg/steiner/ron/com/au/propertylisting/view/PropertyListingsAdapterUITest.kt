package dg.steiner.ron.com.au.propertylisting.view

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import dagger.Component
import dagger.Module
import dagger.Provides
import dg.steiner.ron.com.au.propertylisting.R.id
import dg.steiner.ron.com.au.propertylisting.R.layout
import dg.steiner.ron.com.au.propertylisting.TestActivity
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingResponse
import dg.steiner.ron.com.au.propertylisting.utils.ImageLoadHelper
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subscribers.TestSubscriber
import junit.framework.Assert.assertFalse
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import tests.utils.*


@RunWith(AndroidJUnit4::class)
class PropertyListingsAdapterUITest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(TestActivity::class.java, true, false) //dont launch it

    private val testUtils: TestUtils = TestUtils()

    // mock two PropertyListingListItem to use in the tests
    var items = Array<PropertyListingListViewModel>(2) { _ -> mockk() }

    // a module providing the mocked dependencies
    lateinit var mocks: PropertyListingsAdapterTest.Mocks

    lateinit var listingResponse: ListingResponse

    // SUT
    lateinit var adapter: PropertyListingsAdapter

    // a mocked subscriber to verify emissions
    @RelaxedMockK
    lateinit var subscriber: Subscriber<ListingItem>

    @Before
    fun setUp() {

        testUtils.convertAllSchedulersToTest()
        MockKAnnotations.init(this)

        val activity =
            activityRule.launchActivity(
                TestActivity.intentForLayout(
                    layout.fragment_propertiy_listings,
                    InstrumentationRegistry.getTargetContext()
                )
            )
        listingResponse = testUtils.objectFromStream(
            InstrumentationRegistry.getContext().resources.openRawResource(
                dg.steiner.ron.com.au.propertylisting.test.R.raw.listing_response
            ),
            ListingResponse::class.java
        )!!
        // set the first item as a premium listing and the second item normal:
        with(items[0]) {
            every { isPremium } returns true
            every { primaryText } returns THE_PRIMARY_TEXT_FOR_PREMIUM_LISTING // from the tests.utils package
            every { secondaryText } returns THE_SECONDARY_TEXT_FOR_PREMIUM_LISTING
            every { tertiaryText } returns THE_TERTIARY_TEXT_FOR_PREMIUM_LISTING
            every { firstImageUrl } returns THE_1ST_IMAGE_FOR_PREMIUM_LISTING
            every { secondImageUrl } returns THE_2ND_IMAGE_FOR_PREMIUM_LISTING
            every { agencyLogoUrl } returns THE_AGENCY_LOGO_URL
            every { item } returns listingResponse.listingResults?.listings?.get(0)
        }
        with(items[1]) {
            every { isPremium } returns false
            every { primaryText } returns THE_PRIMARY_TEXT_FOR_REGULAR_LISTING
            every { secondaryText } returns THE_SECONDARY_TEXT_FOR_REGULAR_LISTING
            every { tertiaryText } returns THE_TERTIARY_TEXT_FOR_REGULAR_LISTING
            every { firstImageUrl } returns THE_IMAGE_FOR_REGULAR_LISTING
            every { secondImageUrl } returns null
            every { agencyLogoUrl } returns THE_AGENCY_LOGO_URL
            every { item } returns listingResponse.listingResults?.listings?.get(1)
        }

        mocks = PropertyListingsAdapterTest.Mocks()

        adapter = PropertyListingsAdapter()
        DaggerPropertyListingsAdapterUITest_TestComponent
            .builder()
            .mocks(mocks)
            .build()
            .inject(adapter)

        adapter.items = items

        val recyclerView: RecyclerView = activity.findViewById(id.recycler_view)
        val runnable = Runnable {
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.adapter = adapter
        }
        activity.runOnUiThread(runnable)
    }

    @After
    fun tearDown() {
        testUtils.resetSchedulers()
    }

    @Test
    fun that_list_captures_a_click_on_the_first_item() {

        // use a test-subscriber to wrap calls to mock
        val testSubscriber: TestSubscriber<ListingItem> =
            TestSubscriber.create(object : Subscriber<ListingItem> {
                override fun onComplete() =
                    subscriber.onComplete()

                override fun onSubscribe(s: Subscription?) =
                    subscriber.onSubscribe(s)

                override fun onNext(t: ListingItem) =
                    subscriber.onNext(t)

                override fun onError(t: Throwable?) =
                    subscriber.onError(t)
            })

        adapter.itemClicks
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(testSubscriber)


        // click on the premium item
        onView(withId(id.list_item_premium)).perform(click())

        testUtils.testScheduler.triggerActions()

        // assert the publisher is not completed yet
        assertFalse(adapter.itemClicks.test().isTerminated)
        assertFalse(adapter.itemClicks.test().isDisposed)

        // verify a single emission of the first item in the result
        verify(exactly = 1) { subscriber.onSubscribe(any()) }
        verify(exactly = 1) { subscriber.onNext(listingResponse.listingResults?.listings?.get(0)) }
        verify(exactly = 0) { subscriber.onComplete() }
        verify(exactly = 0) { subscriber.onError(any()) }
    }

    /* a component that injects mocks */
    @Component(modules = [PropertyListingsAdapterTest.Mocks::class])
    interface TestComponent {
        fun inject(adapter: PropertyListingsAdapter)
    }

    /* a module with all the mocks */
    @Module
    class Mocks {

        init {
            MockKAnnotations.init(this)
            every { imageLoadHelper.load(any(), any()) } just Runs
        }

        @MockK
        lateinit var imageLoadHelper: ImageLoadHelper

        @Provides
        fun provideImageLoadHelper(): ImageLoadHelper = imageLoadHelper
    }
}

