package dg.steiner.ron.com.au.propertylisting.model.objects


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

import java.io.Serializable

/*
sampled from the API/provided url and converted using http://www.jsonschema2pojo.org/

This represents a single listing/property item in a list of property listing
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(
    "AdId",
    "AgencyColour",
    "AgencyContactPhoto",
    "AgencyID",
    "AgencyLogoUrl",
    "Area",
    "AuctionDate",
    "AvailableFrom",
    "Bathrooms",
    "Bedrooms",
    "Carspaces",
    "DateFirstListed",
    "DateUpdated",
    "Description",
    "DisplayPrice",
    "DisplayableAddress",
    "EnquiryTimeStamp",
    "Features",
    "FreshnessLevel",
    "GroupCount",
    "GroupNo",
    "HasEnhancedVideoUrl",
    "Headline",
    "HomepassEnabled",
    "ImageUrls",
    "Images",
    "InspectionDate",
    "Inspections",
    "IsBranded",
    "IsElite",
    "IsPriority",
    "Latitude",
    "ListingPrice",
    "ListingStatistics",
    "ListingType",
    "ListingTypeString",
    "Longitude",
    "MapCertainty",
    "Mode",
    "ProjectDetails",
    "PropertyType",
    "PropertyTypeList",
    "Region",
    "RetinaDisplayThumbUrl",
    "SecondRetinaDisplayThumbUrl",
    "SecondThumbUrl",
    "State",
    "StatusLabel",
    "Suburb",
    "ThumbUrl",
    "TopSpotEligible",
    "TruncatedDescription",
    "UnderOfferOrContract",
    "VideoUrl"
)
class ListingItem : Serializable {

    @JsonProperty("AdId")
    @get:JsonProperty("AdId")
    @set:JsonProperty("AdId")
    var adId: Int? = null

    @JsonProperty("AgencyColour")
    @get:JsonProperty("AgencyColour")
    @set:JsonProperty("AgencyColour")
    var agencyColour: String? = null

    @JsonProperty("AgencyContactPhoto")
    @get:JsonProperty("AgencyContactPhoto")
    @set:JsonProperty("AgencyContactPhoto")
    var agencyContactPhoto: String? = null

    @JsonProperty("AgencyID")
    @get:JsonProperty("AgencyID")
    @set:JsonProperty("AgencyID")
    var agencyID: Int? = null

    @JsonProperty("AgencyLogoUrl")
    @get:JsonProperty("AgencyLogoUrl")
    @set:JsonProperty("AgencyLogoUrl")
    var agencyLogoUrl: String? = null

    @JsonProperty("Area")
    @get:JsonProperty("Area")
    @set:JsonProperty("Area")
    var area: String? = null

    @JsonProperty("AuctionDate")
    @get:JsonProperty("AuctionDate")
    @set:JsonProperty("AuctionDate")
    var auctionDate: String? = null

    @JsonProperty("AvailableFrom")
    @get:JsonProperty("AvailableFrom")
    @set:JsonProperty("AvailableFrom")
    var availableFrom: Any? = null

    @JsonProperty("Bathrooms")
    @get:JsonProperty("Bathrooms")
    @set:JsonProperty("Bathrooms")
    var bathrooms: Int? = null

    @JsonProperty("Bedrooms")
    @get:JsonProperty("Bedrooms")
    @set:JsonProperty("Bedrooms")
    var bedrooms: Int? = null

    @JsonProperty("Carspaces")
    @get:JsonProperty("Carspaces")
    @set:JsonProperty("Carspaces")
    var carspaces: Int? = null

    @JsonProperty("DateFirstListed")
    @get:JsonProperty("DateFirstListed")
    @set:JsonProperty("DateFirstListed")
    var dateFirstListed: String? = null

    @JsonProperty("DateUpdated")
    @get:JsonProperty("DateUpdated")
    @set:JsonProperty("DateUpdated")
    var dateUpdated: String? = null

    @JsonProperty("Description")
    @get:JsonProperty("Description")
    @set:JsonProperty("Description")
    var description: String? = null

    @JsonProperty("DisplayPrice")
    @get:JsonProperty("DisplayPrice")
    @set:JsonProperty("DisplayPrice")
    var displayPrice: String? = null

    @JsonProperty("DisplayableAddress")
    @get:JsonProperty("DisplayableAddress")
    @set:JsonProperty("DisplayableAddress")
    var displayableAddress: String? = null

    @JsonProperty("EnquiryTimeStamp")
    @get:JsonProperty("EnquiryTimeStamp")
    @set:JsonProperty("EnquiryTimeStamp")
    var enquiryTimeStamp: Any? = null

    @JsonProperty("Features")
    @get:JsonProperty("Features")
    @set:JsonProperty("Features")
    var features: List<Feature>? = null

    @JsonProperty("FreshnessLevel")
    @get:JsonProperty("FreshnessLevel")
    @set:JsonProperty("FreshnessLevel")
    var freshnessLevel: Int? = null

    @JsonProperty("GroupCount")
    @get:JsonProperty("GroupCount")
    @set:JsonProperty("GroupCount")
    var groupCount: Int? = null

    @JsonProperty("GroupNo")
    @get:JsonProperty("GroupNo")
    @set:JsonProperty("GroupNo")
    var groupNo: Int? = null

    @JsonProperty("HasEnhancedVideoUrl")
    @get:JsonProperty("HasEnhancedVideoUrl")
    @set:JsonProperty("HasEnhancedVideoUrl")
    var hasEnhancedVideoUrl: Int? = null

    @JsonProperty("Headline")
    @get:JsonProperty("Headline")
    @set:JsonProperty("Headline")
    var headline: String? = null

    @JsonProperty("HomepassEnabled")
    @get:JsonProperty("HomepassEnabled")
    @set:JsonProperty("HomepassEnabled")
    var homepassEnabled: Boolean? = null

    @JsonProperty("ImageUrls")
    @get:JsonProperty("ImageUrls")
    @set:JsonProperty("ImageUrls")
    var imageUrls: List<String>? = null

    @JsonProperty("Images")
    @get:JsonProperty("Images")
    @set:JsonProperty("Images")
    var images: Any? = null

    @JsonProperty("InspectionDate")
    @get:JsonProperty("InspectionDate")
    @set:JsonProperty("InspectionDate")
    var inspectionDate: String? = null

    @JsonProperty("Inspections")
    @get:JsonProperty("Inspections")
    @set:JsonProperty("Inspections")
    var inspections: Any? = null

    @JsonProperty("IsBranded")
    @get:JsonProperty("IsBranded")
    @set:JsonProperty("IsBranded")
    var isBranded: Int? = null

    @JsonProperty("IsElite")
    @get:JsonProperty("IsElite")
    @set:JsonProperty("IsElite")
    var isElite: Int? = null

    @JsonProperty("IsPriority")
    @get:JsonProperty("IsPriority")
    @set:JsonProperty("IsPriority")
    var isPriority: Int? = null

    @JsonProperty("Latitude")
    @get:JsonProperty("Latitude")
    @set:JsonProperty("Latitude")
    var latitude: Double? = null

    @JsonProperty("ListingPrice")
    @get:JsonProperty("ListingPrice")
    @set:JsonProperty("ListingPrice")
    var listingPrice: Any? = null

    @JsonProperty("ListingStatistics")
    @get:JsonProperty("ListingStatistics")
    @set:JsonProperty("ListingStatistics")
    var listingStatistics: Any? = null

    @JsonProperty("ListingType")
    @get:JsonProperty("ListingType")
    @set:JsonProperty("ListingType")
    var listingType: String? = null

    @JsonProperty("ListingTypeString")
    @get:JsonProperty("ListingTypeString")
    @set:JsonProperty("ListingTypeString")
    var listingTypeString: String? = null

    @JsonProperty("Longitude")
    @get:JsonProperty("Longitude")
    @set:JsonProperty("Longitude")
    var longitude: Double? = null

    @JsonProperty("MapCertainty")
    @get:JsonProperty("MapCertainty")
    @set:JsonProperty("MapCertainty")
    var mapCertainty: Int? = null

    @JsonProperty("Mode")
    @get:JsonProperty("Mode")
    @set:JsonProperty("Mode")
    var mode: Any? = null

    @JsonProperty("ProjectDetails")
    @get:JsonProperty("ProjectDetails")
    @set:JsonProperty("ProjectDetails")
    var projectDetails: Any? = null

    @JsonProperty("PropertyType")
    @get:JsonProperty("PropertyType")
    @set:JsonProperty("PropertyType")
    var propertyType: String? = null

    @JsonProperty("PropertyTypeList")
    @get:JsonProperty("PropertyTypeList")
    @set:JsonProperty("PropertyTypeList")
    var propertyTypeList: Any? = null

    @JsonProperty("Region")
    @get:JsonProperty("Region")
    @set:JsonProperty("Region")
    var region: String? = null

    @JsonProperty("RetinaDisplayThumbUrl")
    @get:JsonProperty("RetinaDisplayThumbUrl")
    @set:JsonProperty("RetinaDisplayThumbUrl")
    var retinaDisplayThumbUrl: String? = null

    @JsonProperty("SecondRetinaDisplayThumbUrl")
    @get:JsonProperty("SecondRetinaDisplayThumbUrl")
    @set:JsonProperty("SecondRetinaDisplayThumbUrl")
    var secondRetinaDisplayThumbUrl: String? = null

    @JsonProperty("SecondThumbUrl")
    @get:JsonProperty("SecondThumbUrl")
    @set:JsonProperty("SecondThumbUrl")
    var secondThumbUrl: String? = null

    @JsonProperty("State")
    @get:JsonProperty("State")
    @set:JsonProperty("State")
    var state: String? = null

    @JsonProperty("StatusLabel")
    @get:JsonProperty("StatusLabel")
    @set:JsonProperty("StatusLabel")
    var statusLabel: String? = null

    @JsonProperty("Suburb")
    @get:JsonProperty("Suburb")
    @set:JsonProperty("Suburb")
    var suburb: String? = null

    @JsonProperty("ThumbUrl")
    @get:JsonProperty("ThumbUrl")
    @set:JsonProperty("ThumbUrl")
    var thumbUrl: String? = null

    @JsonProperty("TopSpotEligible")
    @get:JsonProperty("TopSpotEligible")
    @set:JsonProperty("TopSpotEligible")
    var topSpotEligible: Int? = null

    @JsonProperty("TruncatedDescription")
    @get:JsonProperty("TruncatedDescription")
    @set:JsonProperty("TruncatedDescription")
    var truncatedDescription: String? = null

    @JsonProperty("UnderOfferOrContract")
    @get:JsonProperty("UnderOfferOrContract")
    @set:JsonProperty("UnderOfferOrContract")
    var underOfferOrContract: Int? = null

    @JsonProperty("VideoUrl")
    @get:JsonProperty("VideoUrl")
    @set:JsonProperty("VideoUrl")
    var videoUrl: Any? = null


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPropertyOrder("Name", "Value")
    class Feature : Serializable {

        @JsonProperty("Name")
        @get:JsonProperty("Name")
        @set:JsonProperty("Name")
        var name: String? = null

        @JsonProperty("Value")
        @get:JsonProperty("Value")
        @set:JsonProperty("Value")
        var value: String? = null

    }
}