package dg.steiner.ron.com.au.propertylisting.model

import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.properties.Delegates

@Singleton
class ListingModel {
    @Inject
    internal constructor(client: ListingClient) {
        this.listingClient = client
    }

    internal constructor()

    private companion object {
        // a real singleton of the cache, shared across model instances
        private val cacheRepo = CacheRepo()

    }

    data class FilterParameters(
        var mode: String,
        var suburb: String,
        var code: String,
        var state: String
    )

    /**
     * a publisher of the currently selected property/listing
     * */
    val selectedItemPublisher: PublishSubject<ListingItem> = PublishSubject.create()


    var selectedItem: ListingItem? by Delegates.observable(null) { _, _: ListingItem?, new: ListingItem? ->
        new?.let {
            selectedItemPublisher.onNext(it)
        }
    }

    /**
     * clients of the model can modify the search using the filter-params instance
     */
    // those values are set for the purpose of the exercise - imagine them being modified by the user via a dedicated view....
    val filterParameters: FilterParameters = FilterParameters(
        "buy",
        "Bondi",
        "2026",
        "NSW"
    )


    @Inject
    internal lateinit var listingClient: ListingClient

    /**
     * fetch the property listings with the current set of filtering params
     * @param ignoreCache - choose to ignore the cache e.g. when the user tries to refresh
     * */
    fun fetchPropertyListings(ignoreCache: Boolean = false): Flowable<ListingItem> {
        // with the current filter parameters  - fetch listings
        with(filterParameters) {
            val singleResponse = listingClient.fetchPropertyListings(mode, suburb, code, state)
                .map { response ->
                    response.listingResults?.listings
                        ?: emptyList() /* nulls not allowed in streams */
                }
                .flatMap(::cacheAndContinue)

            val flowable: Flowable<List<ListingItem>> = if (ignoreCache) {
                // just the network
                singleResponse.toFlowable()
            } else {
                // try to fetch it from the cache and the network
                val maybeFromApi = singleResponse.toMaybe()
                // the im-memory cache might have this list
                val maybeFromCache: Maybe<List<ListingItem>> =
                    cachedListForParams(this/*the filter params*/)
                // the first to return a value will make it to the subscriber (e.g presenter)
                val cacheOrApi = Maybe.concat(maybeFromCache, maybeFromApi)
                    .firstElement() // if the cache calls `onComplete` the network call will not execute
                cacheOrApi
                    .toFlowable()
            }

            return flowable.flatMap { listings ->
                if (listings.isNotEmpty()) Flowable.fromIterable(
                    listings
                ) else Flowable.empty()
            }
        }
    }


    private fun cacheAndContinue(list: List<ListingItem>): Single<List<ListingItem>> {
        cacheRepo.cacheIn(keyFor(filterParameters), list)
        return Single.just(list)
    }

    private fun cachedListForParams(filterParameters: FilterParameters): Maybe<List<ListingItem>> {
        val cacheOut = cacheRepo.cacheOut(keyFor(filterParameters))
        return if (cacheOut != null) Maybe.just(cacheOut) else Maybe.empty()
    }

    private fun keyFor(filterParameters: FilterParameters): String = with(filterParameters) {
        return listOfNotNull(state, suburb, code, mode).joinToString(":")
    }

    private class CacheRepo {
        // a concurrent, thread-safe hash-map to store pre-fetched lists of listing
        // keyed by yhe filter parameters that yielded them
        val cache: ConcurrentHashMap<String, CacheEntry> = ConcurrentHashMap()

        // an entry in the cache that also specifies a time-stamp of it's creation (if cache age is to be considered)
        private data class CacheEntry(val data: List<ListingItem>) {
            val timestamp: Long = System.currentTimeMillis()
        }

        // cache something
        fun cacheIn(key: String, data: List<ListingItem>) {
            cache[key] = CacheEntry(data)
        }

        // retrieve from cache
        fun cacheOut(key: String, noOlderThan: Long = 0): List<ListingItem>? {

            val cacheEntry = cache[key]
            cacheEntry?.let {
                return if (validCacheAge(noOlderThan, cacheEntry)) cacheEntry.data else null
            }
            return null
        }

        private fun validCacheAge(
            noOlderThan: Long,
            cacheEntry: CacheEntry
        ) = noOlderThan > 0 && noOlderThan <= System.currentTimeMillis() - cacheEntry.timestamp

    }
}

