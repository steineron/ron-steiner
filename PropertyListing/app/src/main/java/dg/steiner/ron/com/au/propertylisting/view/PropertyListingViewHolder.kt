package dg.steiner.ron.com.au.propertylisting.view

import android.support.annotation.Nullable
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import dg.steiner.ron.com.au.propertylisting.R
import dg.steiner.ron.com.au.propertylisting.view.custom.AppImageView

class PropertyListingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var root: View

    init {
        ButterKnife.bind(this, itemView)
        // the root is key as the target of interactions (clicks)
        // binding it directly as the id may change between standard/premium listing
        with(itemView) {
            root = findViewById<View>(R.id.list_item_premium) ?:
                    findViewById(R.id.list_item_regular) ?: itemView
        }
    }

    @BindView(R.id.listing_preview_image_primary)
    lateinit var primaryImage: AppImageView

    @BindView(R.id.listing_preview_image_secondary)
    @Nullable
    @JvmField
    var secondaryImage: AppImageView? = null // may be null on regular listings views


    @BindView(R.id.agency_logo)
    lateinit var agencyLogoImage: AppImageView

    @BindView(R.id.primary_text)
    lateinit var primaryText: TextView

    @BindView(R.id.secondary_text)
    lateinit var secondaryText: TextView

    @BindView(R.id.tertiary_text)
    lateinit var tertiaryText: TextView

    @BindView(R.id.agency_color_highlight)
    lateinit var colorHighlight: View


}