package dg.steiner.ron.com.au.propertylisting.view.mediators

import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import dg.steiner.ron.com.au.propertylisting.R
import kotlin.properties.Delegates
/**
 * simple mediator that governs a textview and sets the item's id to it*/
class SelectedPropertyViewMediator(root: View) : ViewMediator(root) {
    init {
        ButterKnife.bind(this, root)
    }

    @BindView(R.id.selected_item)
    lateinit var text: TextView

    var itemId: Int? by Delegates.observable(null) { _, _: Int?, new: Int? ->
        text.text = new?.toString()
    }
}
