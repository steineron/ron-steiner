package dg.steiner.ron.com.au.propertylisting.dependencies

import android.net.Uri
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import dagger.Component
import dagger.Module
import dagger.Provides
import dg.steiner.ron.com.au.propertylisting.BuildConfig
import dg.steiner.ron.com.au.propertylisting.model.ListingClient
import dg.steiner.ron.com.au.propertylisting.model.ListingModel
import dg.steiner.ron.com.au.propertylisting.utils.ImageLoadHelper
import dg.steiner.ron.com.au.propertylisting.view.custom.AppImageView
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

}

@Module
class ApplicationModule {

    private object ImageLoader : ImageLoadHelper {
        @Suppress("DEPRECATION") // setImageURI is deprecated but is undeprecated by Fresco - so suppress this warning while using fresco
        override fun load(url: String?, imageView: AppImageView?) {
            url?.let {
                imageView?.setImageURI(Uri.parse(it))
            }
        }
    }

    companion object {
        // make sure to have only one (singleton)
        private val listingClient: ListingClient by lazy {
            val mapper = ObjectMapper()
                .registerModule(KotlinModule())
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES)

            Retrofit.Builder()
                .baseUrl(BuildConfig.REST_URL_BASE + "/")
                .addConverterFactory(
                    JacksonConverterFactory.create(mapper)
                )
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())) // provide a default scheduler for network calls
                .client(OkHttpClient.Builder().build())
                .build()
                .create(ListingClient::class.java)
        }
        /*
        a real, thread-safe singleton of the model within this application module.
        functionality is similar to Android's ViewModelProviders - it retains and re-provides an instance
        this will help retaining the cache between configuration changes of activities/fragments that use this module
        * */
        private val listingsModel: ListingModel by lazy {
            ListingModel(listingClient)
        }

    }

    @Provides
    fun provideListingClient(): ListingClient = listingClient

    @Provides
    fun provideImageLoadHelper(): ImageLoadHelper = ImageLoader

    @Provides
    fun provideListingModel() = listingsModel
}
