package dg.steiner.ron.com.au.propertylisting.utils

import dg.steiner.ron.com.au.propertylisting.view.custom.AppImageView

/*
* a helper to load images from a url to an ImageView.
* abstract the task's specific by exposing a simple methods that takes an AppImageView and a URL
* and loads it into it
*
* */

interface ImageLoadHelper {

    fun load(url:String? ,imageView: AppImageView?)
}