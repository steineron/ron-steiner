package dg.steiner.ron.com.au.propertylisting.presenter

import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.view.mediators.SelectedPropertyViewMediator
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import javax.inject.Inject

/**
 * a presenter for the currently selected property listing
 * */
class SelectedPropertyPresenter : Presenter, PropertiesPresenter() {

    @Inject
    lateinit var viewMediator: SelectedPropertyViewMediator

    private val itemObserver: Consumer<ListingItem> =
        Consumer { t -> viewMediator.itemId = t?.adId }

    private var disposable: Disposable? = null

    override fun start() {
        // keep the disposable
        disposable = listingModel.selectedItemPublisher.subscribe(itemObserver)
    }

    override fun stop() {
        disposable?.run {
            disposable!!.dispose()
            disposable = null
        }
    }
}
