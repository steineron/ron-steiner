package dg.steiner.ron.com.au.propertylisting.presenter

import dg.steiner.ron.com.au.propertylisting.model.ListingModel
import javax.inject.Inject
/*
* The base class for presenter of data from the listing's Model
* */
open class PropertiesPresenter {
    @Inject
    lateinit var listingModel: ListingModel
}