package dg.steiner.ron.com.au.propertylisting.model.objects

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder("ListingResults")
class ListingResponse : Serializable {

    @JsonProperty("ListingResults")
    @get:JsonProperty("ListingResults")
    @set:JsonProperty("ListingResults")
    var listingResults: ListingResults? = null


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPropertyOrder("Listings")
    inner class ListingResults : Serializable {

        @JsonProperty("Listings")
        @get:JsonProperty("Listings")
        @set:JsonProperty("Listings")
        var listings: List<ListingItem>? = null


    }

}