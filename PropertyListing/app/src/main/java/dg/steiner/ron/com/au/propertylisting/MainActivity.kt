package dg.steiner.ron.com.au.propertylisting

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

private const val FRAGMENT_TAG = "PROPERTIES_LIST_FRAGMENT"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
            .add(R.id.content_root, PropertiesListFragment(), FRAGMENT_TAG)
            .commit()
    }
}
