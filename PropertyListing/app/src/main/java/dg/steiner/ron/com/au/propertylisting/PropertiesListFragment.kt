package dg.steiner.ron.com.au.propertylisting

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.Component
import dagger.Module
import dagger.Provides
import dg.steiner.ron.com.au.propertylisting.dependencies.ApplicationModule
import dg.steiner.ron.com.au.propertylisting.presenter.Presenter
import dg.steiner.ron.com.au.propertylisting.presenter.PropertiesListPresenter
import dg.steiner.ron.com.au.propertylisting.presenter.SelectedPropertyPresenter
import dg.steiner.ron.com.au.propertylisting.view.PropertyListingViewHelper
import dg.steiner.ron.com.au.propertylisting.view.helpers.PropertyListingViewHelperImpl
import dg.steiner.ron.com.au.propertylisting.view.mediators.PropertiesListViewMediator
import dg.steiner.ron.com.au.propertylisting.view.mediators.SelectedPropertyViewMediator

/**
 * PropertiesListFragment - a fragment displaying a list of properties (listings)
 * and optionally the selected item (e.g. in the landscape configuration).
 *
 * utilises a presenter and a mediator for each of those elements (list & selected item)
 *
 * it assumes a role within the Controller so
 *  - it depends on nothing - requires no injections from its creator/runtime (or very little in more complex products)
 *  - it can create whatever it deems required for the task including injectors for the injectable.
 * */
class PropertiesListFragment : Fragment() {

    // presenter + view mediators

    // list of property listings
    private lateinit var listingPresenter: PropertiesListPresenter

    private var propertiesListViewMediator: PropertiesListViewMediator? = null

    // the selected item in the list
    private var selectedViewMediator: SelectedPropertyViewMediator? = null

    private lateinit var selectedItemPresenter: SelectedPropertyPresenter

    // just the list of the started (active) presenters
    private lateinit var activePresenters: MutableList<Presenter>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // the layout should have views to display a list of properties
        /// and possibly a view to display the selected one from the list
        val root = inflater.inflate(R.layout.fragment_propertiy_listings, container, false)
        // mediator for the list
        propertiesListViewMediator =
                PropertiesListViewMediator(root)

        // if there's a view for the selected item - create a mediator for it too
        root.findViewById<View>(R.id.selected_item)?.let {
            selectedViewMediator =
                    SelectedPropertyViewMediator(
                        it
                    )
        }
        return root
    }

    override fun onResume() {
        super.onResume()
        listingPresenter = PropertiesListPresenter()

        // collect the presenters to start
        activePresenters = mutableListOf(listingPresenter)

        val injector = DaggerPropertiesListFragment_Injector.builder()
            .provider(
                Provider(
                    activity!!,
                    propertiesListViewMediator!!,
                    selectedViewMediator
                )
            )
            .build()

        injector.inject(listingPresenter)

        // optionally - create presenter for the selected item
        selectedViewMediator?.let {
            selectedItemPresenter =
                    SelectedPropertyPresenter()
            injector.inject(selectedItemPresenter)
            activePresenters.add(selectedItemPresenter)
        }

        for (presenter in activePresenters){
            presenter.start()
        }
    }

    override fun onPause() {
        for (presenter in activePresenters){
            presenter.stop()
        }
        super.onPause()
    }

    override fun onDestroyView() {
        propertiesListViewMediator = null
        selectedViewMediator = null;
        super.onDestroyView()

    }

    @Component(modules = [Provider::class])
    interface Injector {
        fun inject(presenter: PropertiesListPresenter)

        fun inject(presenter: SelectedPropertyPresenter)
    }

    @Module(includes = [ApplicationModule::class])
    class Provider(
        private val activity: Activity,
        private val listViewMediator: PropertiesListViewMediator,
        // if planing to inject itemViewMediator - instantiate this provider with non-null instance of it (or get a runtime exception)
        private val itemViewMediator: SelectedPropertyViewMediator?
    ) {

        @Provides
        fun provideActivity() = activity

        @Provides
        fun providePropertiesListViewMediator() = listViewMediator

        @Provides
        fun provideSelectedItemViewMediator() = itemViewMediator!!

        @Provides
        fun providePropertyListingViewHelper(activity: Activity): PropertyListingViewHelper {
            return PropertyListingViewHelperImpl(activity)
        }
    }

}