package dg.steiner.ron.com.au.propertylisting.view.mediators

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import dagger.Component
import dg.steiner.ron.com.au.propertylisting.R
import dg.steiner.ron.com.au.propertylisting.dependencies.ApplicationModule
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.view.mediators.DaggerPropertiesListViewMediator_Injector
import dg.steiner.ron.com.au.propertylisting.view.PropertyListingListViewModel
import dg.steiner.ron.com.au.propertylisting.view.PropertyListingsAdapter
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlin.properties.Delegates

/*
* a view-mediator for the list of properties
*
* the list of properties is implemented using a recycler-view with a swipe-refresh layout
*
* the view mediator exposes api to:
*   - set a list of items to display
*   - show/hide indicator of progress (loading)
* */
class PropertiesListViewMediator(root: View) : ViewMediator(root) {

    /*
    * these are the interactions that the mediator brokers/reports
    * about
    * */
    interface Interactions {

        // the user attempts a refresh of the view (data)
        fun onUserRefresh()

        // the user clicked on an item in the list
        fun onItemClicked(item: ListingItem)
    }


    var propertiesListing: Array<PropertyListingListViewModel>? by Delegates.observable(emptyArray()) { _, old, new ->
        /*
        * whenever a new list of items is set, this observable is triggered.
        *
        * it uses a utility class to compute the changes between the existing list of items and the new list of items,
         * and ends by dispatching a series of `notifyXYZHappened` to the adapter, resulting in a smooth, well animated, focused change in the view
        * */

        val callback: DiffUtil.Callback = object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean =
                old?.get(oldPosition)?.item?.adId == new?.get(newPosition)?.item?.adId

            override fun getOldListSize(): Int = old?.size ?: 0

            override fun getNewListSize(): Int = new?.size ?: 0

            override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean =
                old?.get(oldPosition)?.comparableDescription == new?.get(newPosition)?.comparableDescription
        }
        // take the computation of diff off the main thread
        Single.fromCallable { DiffUtil.calculateDiff(callback) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { diff ->
                //lastly - assign and dispatch the updates
                recyclerViewAdapter.items = new ?: emptyArray()
                diff.dispatchUpdatesTo(recyclerViewAdapter)
            }
    }

    var interactions: Interactions? = null

    private val recyclerViewAdapter: PropertyListingsAdapter =
        PropertyListingsAdapter()

    init {
        ButterKnife.bind(this, root)

        recyclerViewAdapter.let {
            // inject it
            DaggerPropertiesListViewMediator_Injector.builder()
                .build()
                .inject(it)
            // subscribe to emissions of the items user clicked on
            it.itemClicks.subscribe { item -> interactions?.onItemClicked(item) }
        }

        swipeToRefresh.setOnRefreshListener {
            interactions?.onUserRefresh()
        }

        with(recyclerView) {
            layoutManager = LinearLayoutManager(root.context)
            val animator = DefaultItemAnimator()
            animator.supportsChangeAnimations = false
            itemAnimator = animator
            adapter = recyclerViewAdapter
        }
    }

    @BindView(R.id.swipe_refresh)
    lateinit var swipeToRefresh: SwipeRefreshLayout

    @BindView(R.id.recycler_view)
    lateinit var recyclerView: RecyclerView

    /*
    * show/hide progress indicator
    *
    * the progress indicator in the view is the swipe-refresh progressbar, accessible via SwipeRefreshLayout.isRefreshing
    * */
    var inProgress: Boolean by Delegates.observable(false) { _, _, new ->
        swipeToRefresh.isRefreshing = new
    }

    fun displayError() =
        Toast.makeText(root.context, R.string.msg_error_on_load, Toast.LENGTH_SHORT).show()

    @Component(modules = [ApplicationModule::class])
    interface Injector {
        fun inject(adapter: PropertyListingsAdapter)
    }
}