package dg.steiner.ron.com.au.propertylisting.view.helpers

import android.app.Activity
import android.graphics.drawable.ColorDrawable
import dg.steiner.ron.com.au.propertylisting.R
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.view.PropertyListingViewHelper
import java.lang.ref.WeakReference
import java.util.concurrent.ConcurrentHashMap

/*
* a very "simple" helper to convert to title-case*/
fun String.toTitleCase(): String? {
    val text = this
    if (text.isEmpty()) {
        return text
    }

    val converted = StringBuilder()

    var convertNext = true
    for (chr in text.toCharArray()) {
        var c = chr
        when {
            Character.isSpaceChar(c) -> convertNext = true
            convertNext -> {
                c = Character.toTitleCase(c)
                convertNext = false
            }
            else -> c = Character.toLowerCase(c)
        }
        converted.append(c)
    }

    return converted.toString()
}

class PropertyListingViewHelperImpl(activity: Activity) :
    PropertyListingViewHelper {

    override fun getAgencyColor(item: ListingItem): Int {
        item.agencyColour?.let {
            return ("FF"+if('#' == it[0]) it.slice(1 until it.length) else it).toLong(radix = 16).toInt()
        }
        return 0
    }

    companion object {
        val colorMap:ConcurrentHashMap<String, ColorDrawable> = ConcurrentHashMap()
    }
    private val _activityRef: WeakReference<Activity> = WeakReference(activity)

    private val activity: Activity?
        get() = _activityRef.get()

    override fun getPriceText(item: ListingItem): CharSequence =
        item.displayPrice?.toTitleCase() ?: ""

    override fun getSpecificationsText(item: ListingItem): CharSequence {

        with(item) {
            val texts = mutableListOf<String>()
            activity?.resources?.let { res ->
                bedrooms?.let { rooms ->
                    texts.add(
                        res.getQuantityString(
                            R.plurals.listing_specification_rooms,
                            rooms, rooms
                        )
                    )
                }
                bathrooms?.let { baths ->
                    texts.add(
                        res.getQuantityString(
                            R.plurals.listing_specification_baths,
                            baths, baths
                        )
                    )
                }
                carspaces?.let { cars ->
                    if (carspaces!! > 0) {
                        texts.add(
                            res.getQuantityString(
                                R.plurals.listing_specification_cars,
                                cars, cars
                            )
                        )
                    }
                }
            }
            return texts.joinToString(", ")
        }
    }

    override fun getAddressText(item: ListingItem): CharSequence = item.displayableAddress ?: ""
}
