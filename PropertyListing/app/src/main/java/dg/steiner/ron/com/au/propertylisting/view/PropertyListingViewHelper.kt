package dg.steiner.ron.com.au.propertylisting.view

import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem

/*
* An interface that returns texts to be used in views, e.g. price, number of room address etc.
* */
interface PropertyListingViewHelper {

    fun getPriceText(item: ListingItem): CharSequence

    fun getSpecificationsText(item: ListingItem): CharSequence

    fun getAddressText(item: ListingItem): CharSequence

    fun getAgencyColor(item: ListingItem): Int
}