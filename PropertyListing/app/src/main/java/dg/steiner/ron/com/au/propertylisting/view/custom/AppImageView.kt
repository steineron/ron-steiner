package dg.steiner.ron.com.au.propertylisting.view.custom

import android.content.Context
import android.util.AttributeSet
import com.facebook.drawee.view.SimpleDraweeView

/*
* the current choice of image loading lib is Fresco (http://frescolib.org/). as it is reported to be faster than Picasso and Glide
* (it also supports some nice features such as progressbar, placeholder, corner-radius and more)
* Fresco uses a custom subclass of ImageView (SimpleDraweeView) which should be used in the layout xml files.
* In order to future-proof the need to replace this class everywhere when switching from Fresco library,
* declare a custom, app-specific subclass of ImageView
* i.e. Rather than changing the class name in each and every xml file - simply change
* the parent of this class to (possibly another subclass of) ImageView
* */
class AppImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : SimpleDraweeView(context, attrs, defStyleAttr)