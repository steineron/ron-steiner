package dg.steiner.ron.com.au.propertylisting.view

import android.graphics.drawable.ColorDrawable
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.utils.ComparableDescription
import dg.steiner.ron.com.au.propertylisting.utils.ItemWrapper

/*
* an adapter item to use in PropertyListingsAdapter, wrapping a listing-item (from the server),
* while providing a simple abstraction of the values needed for adapting the model object (ListingItem)
* to the view
*/

/*
* nulls - one item that answers null/false values
* it is used while binding when there is not item to bind (shouldn't happen)
* and views (e.g text views, images etc) should be set with values, or change visibility
* */
internal val nulls:PropertyListingListViewModel by lazy {

     object : PropertyListingListViewModel{
         override val agencyLogoUrl: String?
             get() = null
         override val firstImageUrl: String?
            get() = null
        override val secondImageUrl: String?
            get() = null
        override val isPremium: Boolean
            get() = false
        override val item: ListingItem?
            get() = null
        override val primaryText: CharSequence?
            get() = null
        override val secondaryText: CharSequence?
            get() = null
        override val tertiaryText: CharSequence?
            get() = null
        override val comparableDescription: String
            get() = ""
         override val agencyColor: ColorDrawable
             get() = ColorDrawable(0)
     }
}

/*
* the interface of a property listing item exposing
* - 3 texts,
* - 1-2 image urls, and
* - a boolean for telling apart premium and regular listings
* */
interface PropertyListingListViewModel : ItemWrapper<ListingItem>, ComparableDescription{

    val isPremium: Boolean

    val primaryText: CharSequence?

    val secondaryText: CharSequence?

    val tertiaryText: CharSequence?

    val firstImageUrl: String?

    val secondImageUrl: String?

    val agencyLogoUrl: String?

    val agencyColor:ColorDrawable

}