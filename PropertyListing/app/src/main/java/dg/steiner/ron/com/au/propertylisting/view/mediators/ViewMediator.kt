package dg.steiner.ron.com.au.propertylisting.view.mediators

import android.view.View

open class ViewMediator(val root: View)