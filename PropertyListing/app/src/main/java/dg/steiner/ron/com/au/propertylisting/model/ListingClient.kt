package dg.steiner.ron.com.au.propertylisting.model

import dg.steiner.ron.com.au.propertylisting.model.objects.ListingResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * an interface abstraction of the api client to fetch data from the network
 */
interface ListingClient {

    /**
     * fetch a fresh server response with property listings
     * @param mode - buy/rent
     * @param suburb - desired suburb name
     * @param code - desired postal code
     * @param state - capital-letters state
     *
     * @return - a single listing response
     */
    @GET("searchservice.svc/mapsearch")
    fun fetchPropertyListings(
        @Query("mode") mode: String,
        @Query("sub") suburb: String,
        @Query("pcodes") code: String,
        @Query("state") state: String
    ): Single<ListingResponse>
}
