package dg.steiner.ron.com.au.propertylisting.utils

/*
* when something can be compared based on its description
* */
interface ComparableDescription {
    val comparableDescription: String
}