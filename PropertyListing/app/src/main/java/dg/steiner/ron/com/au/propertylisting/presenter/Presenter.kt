package dg.steiner.ron.com.au.propertylisting.presenter
/**
* the two basic operations required by a presenter - start and stop presenting
* */
interface Presenter {
    fun start()
    fun stop()
}