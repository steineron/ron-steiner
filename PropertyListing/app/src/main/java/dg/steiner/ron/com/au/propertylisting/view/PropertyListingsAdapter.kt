package dg.steiner.ron.com.au.propertylisting.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import dg.steiner.ron.com.au.propertylisting.R
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.utils.ImageLoadHelper
import io.reactivex.processors.PublishProcessor
import javax.inject.Inject

/*
* A recycler view adapter tha adapts listings to view
* */

private const val VIEW_TYPE_PREMIUM_LISTING = 1
private const val VIEW_TYPE_NORMAL_LISTING = VIEW_TYPE_PREMIUM_LISTING shl 1

class PropertyListingsAdapter : RecyclerView.Adapter<PropertyListingViewHolder>() {

    @Inject
    lateinit var imageLoadHelper: ImageLoadHelper

    // a publisher of all the items clicked by the user - enabling one click to be delivered to many (as the data - not the view)
    val itemClicks: PublishProcessor<ListingItem> = PublishProcessor.create()

    // the array of items rendered by this adapter
    var items: Array<PropertyListingListViewModel> = emptyArray()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyListingViewHolder {
        with(parent) {
            val layoutId =
                when (viewType) {
                    VIEW_TYPE_PREMIUM_LISTING -> R.layout.list_item_premium_listing
                    VIEW_TYPE_NORMAL_LISTING -> R.layout.list_item_regular_listing
                    else -> 0
                }

            val inflater = LayoutInflater.from(context)
            // wrapping the actual layout in a decorated container
            // a hacky and effective way to make the view look nice - with elevations and spacing and colors.
            val view = inflater.inflate(R.layout.list_item_container, this,false)
            inflater.inflate(layoutId, view.findViewById(R.id.listing_container))
            return PropertyListingViewHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position in items.indices) {
            return if (items[position].isPremium) VIEW_TYPE_PREMIUM_LISTING else VIEW_TYPE_NORMAL_LISTING
        }
        return 0
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(vh: PropertyListingViewHolder, position: Int) {
        val adapterItem: PropertyListingListViewModel =
            if (position in items.indices) items[position] else nulls /*nulls - an actual implementation that returns nulls/falses*/

        // using 'nulls' here helps "resetting" views rather than leaving them to display previously bound values
        // or alternatively implement an "ugly" else for when item==null
        with(adapterItem) {
            vh.let { its->
                // emmit the item when the user clicks
                its.root.setOnClickListener { _ -> itemClicks.onNext(item!!)}
                // set all the texts, load images
                its.primaryText.text = primaryText
                its.secondaryText.text = secondaryText
                its.tertiaryText.text = tertiaryText
                imageLoadHelper.load(firstImageUrl, its.primaryImage)
                imageLoadHelper.load(secondImageUrl, its.secondaryImage)
                imageLoadHelper.load(agencyLogoUrl, its.agencyLogoImage)
                its.colorHighlight.background = agencyColor
            }
        }
    }
}
