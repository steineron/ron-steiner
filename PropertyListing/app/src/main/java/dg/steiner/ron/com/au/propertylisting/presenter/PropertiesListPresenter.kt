package dg.steiner.ron.com.au.propertylisting.presenter

import android.graphics.drawable.ColorDrawable
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.view.PropertyListingListViewModel
import dg.steiner.ron.com.au.propertylisting.view.PropertyListingViewHelper
import dg.steiner.ron.com.au.propertylisting.view.mediators.PropertiesListViewMediator
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


/*
* a presenter for the list properties.
*
* subscribes to the model for a flow of listings and assigns them to the mediator
*
* the presenter has a semi-intimate knowledge about the view and how to work with it:
* - it does know that the view needs a list of items, each exposing a small set of attributes
* - it knows what to do when the user interacts with the views
* - it doesn't know which ui-elements (Views) are used
*
* the presenter will:
* - fetch data for the view, and convert this data to a view-friendly/oriented one
* - show and hide progress indicator while fetching data
* - display an error to the user if needed.
*
* and it will do all that via a mediator that actually wraps the view.
*
* p.s. this isn't the classic P in MVP
* */
class PropertiesListPresenter : Presenter, PropertiesPresenter() {

    val interactions = object :
        PropertiesListViewMediator.Interactions {
        override fun onUserRefresh() {
            fetchPropertyListings(true /*explicit refresh - ignore the cache*/)
        }

        override fun onItemClicked(item: ListingItem) {
            listingModel.selectedItem = item
        }
    }

    /*
    * An implementation of PropertyListingListViewModel exposes a view-friendly api.
    * */
    private data class PropertyViewModel(
        override val item: ListingItem?,
        override val isPremium: Boolean,
        override val primaryText: CharSequence?,
        override val secondaryText: CharSequence?,
        override val tertiaryText: CharSequence?,
        override val firstImageUrl: String?,
        override val secondImageUrl: String?,
        override val agencyLogoUrl: String?,
        override val agencyColor: ColorDrawable
    ) : PropertyListingListViewModel {
        override val comparableDescription: String
            get() = // consider all the things that are displayed in hte view (content) and form a "uri" of them
                listOf(
                    primaryText,
                    secondaryText,
                    tertiaryText,
                    isPremium.toString(),
                    firstImageUrl,
                    secondImageUrl
                ).joinToString(":")
    }

    @Inject
    lateinit var viewMediator: PropertiesListViewMediator

    @Inject
    lateinit var propertyListingViewHelper: PropertyListingViewHelper

    // an observer for the single list of PropertyViewModels emitted by successful fetch
    private var listingsConsumer: DisposableSingleObserver<List<PropertyListingListViewModel>>? =
        null

    override fun start() {
        viewMediator.interactions = interactions
        fetchPropertyListings() // does not ignore the cache - e.g. when the activity is resumed after a configuration-change
    }

    override fun stop() {
        listingsConsumer?.dispose()
        listingsConsumer = null
        viewMediator.interactions = null
    }

    /**
     * pre-compute everything the adapter will need so that binding is fast and does NOT allocate *anything* on the main thread*/
    internal fun itemToModel(item: ListingItem): PropertyListingListViewModel {
        // the texts to be displayed for this item are obtained wit the help of
        // PropertyListingViewHelper
        with(propertyListingViewHelper) {
            return PropertyViewModel(
                item,
                item.isElite != 0,
                getPriceText(item),
                getSpecificationsText(item),
                getAddressText(item),
                item.retinaDisplayThumbUrl,
                item.secondRetinaDisplayThumbUrl,
                item.agencyLogoUrl,
                ColorDrawable(getAgencyColor(item))
            )
        }
    }

    private fun fetchPropertyListings(ignoreCache: Boolean = false) {
        listingsConsumer?.run {
            with(listingsConsumer!!) {
                if (!isDisposed)
                    dispose()
            }
        }
        listingsConsumer = object : DisposableSingleObserver<List<PropertyListingListViewModel>>() {
            override fun onStart() {
                viewMediator.inProgress = true
            }

            override fun onSuccess(models: List<PropertyListingListViewModel>) {
                viewMediator.propertiesListing = models.toTypedArray()
                viewMediator.inProgress = false
            }

            override fun onError(e: Throwable) {
                viewMediator.inProgress = false
                viewMediator.displayError()
            }
        }

        // fetch the listings, convert them 1-by-1 to view-models, collect them in a list
        listingModel.fetchPropertyListings(ignoreCache)
            .subscribeOn(Schedulers.io())
            .map(::itemToModel)
            .toList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(listingsConsumer!!)
    }
}