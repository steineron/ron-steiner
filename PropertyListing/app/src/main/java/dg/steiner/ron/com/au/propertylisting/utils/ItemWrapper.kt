package dg.steiner.ron.com.au.propertylisting.utils

/*
simple-wrapper interface for a nullable of type T

usually views will interact with implementors of this interface,
that will provide a view-oriented/friendly values to display, while wrapping the actual element
for other usages,
e.g. displaying an item in a list using 3 texts generated _from_ it,
while using the actual item when navigating to it's detailed view
* */
interface ItemWrapper<T> {
    val item: T?
}