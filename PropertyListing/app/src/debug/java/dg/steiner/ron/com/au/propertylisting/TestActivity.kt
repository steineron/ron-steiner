package dg.steiner.ron.com.au.propertylisting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity

/**
 * this activityRule is for instrumented testing only (hence in the debug folder)
 */

private const val EXTRA_LAYOUT_ID = "EXTRA_LAYOUT_ID"

class TestActivity : AppCompatActivity() {
    companion object {
        fun intentForLayout(@LayoutRes layoutId: Int, context: Context): Intent =
            Intent(context, TestActivity::class.java).putExtra(EXTRA_LAYOUT_ID, layoutId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layoutId = intent.getIntExtra(EXTRA_LAYOUT_ID, -1)
        setContentView(if (layoutId != -1) layoutId else R.layout.activity_test)
    }
}
