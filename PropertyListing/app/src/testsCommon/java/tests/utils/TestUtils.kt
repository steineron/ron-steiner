package tests.utils

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertNotNull
import java.io.InputStream
import javax.inject.Inject
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.jvm.javaField


/**
 * some constant values for items (mocks) used in testing
 */
const val THE_PRIMARY_TEXT_FOR_PREMIUM_LISTING = "premium-primary-text"
const val THE_SECONDARY_TEXT_FOR_PREMIUM_LISTING = "premium-secondary-text"
const val THE_TERTIARY_TEXT_FOR_PREMIUM_LISTING = "premium-tertiary-text"
const val THE_1ST_IMAGE_FOR_PREMIUM_LISTING = "premium-first-image-url"
const val THE_2ND_IMAGE_FOR_PREMIUM_LISTING = "premium-second-image-url"

const val THE_PRIMARY_TEXT_FOR_REGULAR_LISTING = "normal-primary-text"
const val THE_SECONDARY_TEXT_FOR_REGULAR_LISTING = "normal-secondary-text"
const val THE_TERTIARY_TEXT_FOR_REGULAR_LISTING = "normal-tertiary-text"
const val THE_IMAGE_FOR_REGULAR_LISTING = "normal-image-url"

const val THE_AGENCY_LOGO_URL = "logo-url"

class TestUtils {
    internal val testScheduler: TestScheduler = TestScheduler()
    private val objectMapper = ObjectMapper()
        .registerModule(KotlinModule())
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

    fun convertAllSchedulersToTest() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> testScheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { _ -> testScheduler }
        RxJavaPlugins.setIoSchedulerHandler { _ -> testScheduler }
        RxJavaPlugins.setComputationSchedulerHandler { _ -> testScheduler }
        RxJavaPlugins.setNewThreadSchedulerHandler { _ -> testScheduler }
        RxJavaPlugins.setSingleSchedulerHandler { _ -> testScheduler }
    }


    fun resetSchedulers() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }

    fun <T> objectFromResource(resourcePath: String, classs: Class<T>):T? {

        val resource = ClassLoader.getSystemResourceAsStream(resourcePath)

        return objectFromStream(resource, classs)
    }

    fun <T> objectFromStream(stream: InputStream?, classs: Class<T>): T =
        objectMapper.readValue(stream, classs)


    // asset that all Inject-annotated properties have values (post injection)
    fun assertInjections(instance: Any) {
        // collect all inject-annotated properties
        val memberProperties = instance::class.declaredMemberProperties

        val injected = memberProperties
            .filter { property -> property.javaField?.getAnnotation(Inject::class.java) != null }

        assertFalse(injected.isEmpty())
        // assert that they aren't null (injected during setup)
        for (property in injected) {
            val value = property.getter.call(instance)
            assertNotNull(value)
        }
    }
}