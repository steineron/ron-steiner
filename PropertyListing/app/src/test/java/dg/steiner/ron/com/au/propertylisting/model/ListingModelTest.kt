package dg.steiner.ron.com.au.propertylisting.model

import dagger.Component
import dagger.Module
import dagger.Provides
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingResponse
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.spyk
import io.mockk.verify
import io.reactivex.Flowable
import io.reactivex.Single
import junit.framework.Assert.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import tests.utils.TestUtils

class ListingModelTest {

    private val testUtils: TestUtils = TestUtils()

    private val mocks: Mocks = Mocks()

    // SUT
    lateinit var model: ListingModel

    lateinit var listingResponse: ListingResponse


    @Before
    fun setUp() {
        // use test-schedulers
        testUtils.convertAllSchedulersToTest()

        // create the model for test, inject it with mocks
        model = ListingModel()

        DaggerListingModelTest_TestComponent
            .builder()
            .mocks(mocks)
            .build()
            .inject(model)

        // build a response from captured one
        listingResponse = testUtils.objectFromResource(
            "raw/listing_response.json",
            ListingResponse::class.java
        )!!

        // setup the single response the mock-client should return
        val response: Single<ListingResponse> = Single.just(listingResponse)

        every {
            mocks.listingClient.fetchPropertyListings(
                any(),
                any(),
                any(),
                any()
            )
        } returns response
    }

    @After
    fun tearDown() = testUtils.resetSchedulers()

    @Test
    fun that_fetch_uses_correct_filter_values() {

        model.fetchPropertyListings().subscribe()

        testUtils.testScheduler.triggerActions()

        verify {
            mocks.listingClient.fetchPropertyListings(
                eq("buy"),
                eq("Bondi"),
                eq("2026"),
                eq("NSW")
            )
        }

    }

    @Test
    fun that_fetch_returns_a_flowable_of_all_items() {

        val flowable: Flowable<ListingItem> = model.fetchPropertyListings()

        val emitterListing: MutableList<ListingItem> = MutableList(0) { _ -> ListingItem() }
        flowable.subscribe { listingItem -> emitterListing.add(listingItem) }

        testUtils.testScheduler.triggerActions()

        assertTrue(flowable.test().isTerminated)

        // assert that size and order are maintained
        assertTrue(emitterListing.size == listingResponse.listingResults?.listings?.size)
        val zip = emitterListing.zip(listingResponse.listingResults?.listings!!.asIterable())
        for (pair in zip) {
            assertTrue(pair.first == pair.second)
        }
    }

    @Test
    fun that_empty_listing_response_terminates_the_flow() {

        // return an empty list
        listingResponse.listingResults?.listings = emptyList()

        val flowable: Flowable<ListingItem> = model.fetchPropertyListings()

        val emitterListing: MutableList<ListingItem> = MutableList(0) { _ -> ListingItem() }
        flowable.subscribe { listingItem -> emitterListing.add(listingItem) }

        testUtils.testScheduler.triggerActions()

        assertTrue(flowable.test().isTerminated)

        // assert that size and order are maintained
        assertTrue(emitterListing.size == 0)

    }

    @Test
    fun that_null_listing_results_response_terminates_the_flow() {

        // return a null list
        listingResponse.listingResults?.listings = null

        val flowable: Flowable<ListingItem> = model.fetchPropertyListings()

        val emitterListing: MutableList<ListingItem> = MutableList(0) { _ -> ListingItem() }
        val subscriber = spyk(object : Subscriber<ListingItem> {
            override fun onComplete() {
            }

            override fun onSubscribe(s: Subscription?) {
            }

            override fun onNext(t: ListingItem?) {
                t ?: emitterListing.add(t!!)
            }

            override fun onError(t: Throwable?) {
            }
        })
        flowable.subscribe(subscriber)

        testUtils.testScheduler.triggerActions()

        assertTrue(flowable.test().isTerminated)

        // assert that size and order are maintained
        assertTrue(emitterListing.size == 0)

        verify(exactly = 0) { subscriber.onNext(any()) }
        verify(exactly = 1) { subscriber.onComplete() }
        verify(exactly = 0) { subscriber.onError(any()) }

    }

    @Test
    fun that_null_listing_in_response_terminates_the_flow_with_an_error() {

        // return a null result
        listingResponse.listingResults = null

        val flowable: Flowable<ListingItem> = model.fetchPropertyListings()

        val emitterListing: MutableList<ListingItem> = MutableList(0) { _ -> ListingItem() }
        val subscriber = spyk(object : Subscriber<ListingItem> {
            override fun onComplete() {
            }

            override fun onSubscribe(s: Subscription?) {
            }

            override fun onNext(t: ListingItem?) {
                t ?: emitterListing.add(t!!)
            }

            override fun onError(t: Throwable?) {
            }
        })
        flowable.subscribe(subscriber)

        testUtils.testScheduler.triggerActions()

        assertTrue(flowable.test().isTerminated)

        // assert that size and order are maintained
        assertTrue(emitterListing.size == 0)

        verify(exactly = 0) { subscriber.onNext(any()) }
        verify(exactly = 1) { subscriber.onComplete() }
        verify(exactly = 0) { subscriber.onError(any()) }

    }


    @Test
    fun that_model_is_injectable_and_injected() {
        testUtils.assertInjections(model)
    }

    /* a component that injects mocks */
    @Component(modules = [Mocks::class])
    interface TestComponent {
        fun inject(listingModel: ListingModel)
    }

    /* a module with all the mocks */
    @Module
    class Mocks {

        init {
            MockKAnnotations.init(this)
        }

        @MockK
        lateinit var listingClient: ListingClient

        @Provides
        fun provideListingClient(): ListingClient = listingClient
    }
}