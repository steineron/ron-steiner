package dg.steiner.ron.com.au.propertylisting.view

import dagger.Component
import dagger.Module
import dagger.Provides
import dg.steiner.ron.com.au.propertylisting.model.ListingModel
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingResponse
import dg.steiner.ron.com.au.propertylisting.presenter.PropertiesListPresenter
import dg.steiner.ron.com.au.propertylisting.view.mediators.PropertiesListViewMediator
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Flowable
import io.reactivex.FlowableSubscriber
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import tests.utils.TestUtils

class PropertiesListPresenterTest {

    lateinit var testUtils: TestUtils
    // mocks
    lateinit var mocks: PresenterTestMocks
    // SUT
    lateinit var presenter: PropertiesListPresenter

    private lateinit var listingResponse: ListingResponse

    private lateinit var spiedFlowable :Flowable<ListingItem>

    @Before
    fun setUp() {
        testUtils = TestUtils()
        testUtils.convertAllSchedulersToTest()
        mocks = PresenterTestMocks()
        presenter = PropertiesListPresenter()

        DaggerPropertiesListPresenterTest_PresenterTestComponent
            .builder()
            .presenterTestMocks(mocks)
            .build()
            .inject(presenter)

        // build a response from captured one - it will have 10 items
        listingResponse = testUtils.objectFromResource(
            "raw/listing_response.json",
            ListingResponse::class.java
        )!!
        spiedFlowable = spyk(
            Flowable.fromIterable(
                listingResponse.listingResults?.listings
            )
        )

        // return a flowable from the test's data
        every { mocks.listingsModel.fetchPropertyListings() } returns spiedFlowable
    }

    @After
    fun tearDown() {
        testUtils.resetSchedulers()
    }

    @Test
    fun that_presenter_subscribes_to_the_model_when_started() {

        // before calling - nothing interacts with this mock
        verify { spiedFlowable wasNot Called }

        presenter.start()

        testUtils.testScheduler.triggerActions()
        // verify that the presenter actually subscribes
        verify { spiedFlowable.subscribe(any<FlowableSubscriber<ListingItem>>()) }
    }


    @Test
    fun that_presenter_displays_error() {

        // there's some inside knowledge in this test about how the presenter uses the flowable returned by the model's fetchPropertyListings
        val errorFlowable: Flowable<ListingItem> =
            Flowable.fromCallable { throw NullPointerException() }
        every { mocks.listingsModel.fetchPropertyListings() } returns errorFlowable

        every { mocks.listingsViewMediator setProperty "inProgress" value true } just Runs

        presenter.start()

        verify { mocks.listingsViewMediator setProperty "inProgress" value true }

        testUtils.testScheduler.triggerActions()

        verify { mocks.listingsViewMediator setProperty "inProgress" value false }
        verify { mocks.listingsViewMediator.displayError() }
    }


    @Test
    fun that_presenter_fetches_listings_when_started() {

        verify { mocks.listingsModel wasNot Called }

        presenter.start()

        verify { mocks.listingsModel.fetchPropertyListings() }
    }


    @Test
    fun that_presenter_creates_models_from_the_flow_of_items_and_sets_it_on_the_mediator() {

        // capture the value emitted to the mediator by the presenter (subscribing to the mock of the model)
        val slot = slot<Array<PropertyListingListViewModel>>()
        every { mocks.listingsViewMediator setProperty "propertiesListing" value capture(slot) } just Runs

        presenter.start()
        testUtils.testScheduler.triggerActions()

        verify { mocks.listingsModel.fetchPropertyListings() }
        verify { mocks.listingsViewMediator setProperty "propertiesListing" value any<Array<PropertyListingListViewModel>>() }
        val models = slot.captured

        // the presenter should have assigned 10 items to the mediator - reflecting the 10 items in the test's data
        assertThat(models.size, `is`(10))
    }

    @Test
    fun that_presenter_displays_progress_using_the_mediator_just_before_fetching() {

        // capture the value set to the mediator's inProgress before fetching
        val slot = slot<Boolean>()
        every { mocks.listingsViewMediator setProperty "inProgress" value capture(slot) } just Runs

        presenter.start()

        verify { mocks.listingsViewMediator setProperty "inProgress" value true }

    }

    @Test
    fun that_presenter_hides_progress_using_the_mediator_just_after_fetching() {

        // capture the value set to the mediator's inProgress after the fetching concludes
        val slot = slot<Boolean>()
        every { mocks.listingsViewMediator setProperty "inProgress" value capture(slot) } just Runs

        presenter.start()

        verify { mocks.listingsViewMediator setProperty "inProgress" value true }

        testUtils.testScheduler.triggerActions()

        verify { mocks.listingsViewMediator setProperty "inProgress" value false }

    }

    @Test
    fun that_presenter_uses_the_view_helper_to_create_texts_for_the_view() {

        val item = listingResponse.listingResults?.listings?.first()!!

        with(mocks.listingViewHelper) {
            every { getPriceText(item) } returns "The Price"
            every { getSpecificationsText(item) } returns "The Specs"
            every { getAddressText(item) } returns "The Address"
        }

        val listViewModel = presenter.itemToModel(item)

        with(mocks.listingViewHelper) {
            verify { getPriceText(item) }
            verify { getSpecificationsText(item) }
            verify { getAddressText(item) }
        }

        with(listViewModel) {
            assertThat(primaryText.toString(), `is`("The Price"))
            assertThat(secondaryText.toString(), `is`("The Specs"))
            assertThat(tertiaryText.toString(), `is`("The Address"))
            assertThat(firstImageUrl, `is`(item.retinaDisplayThumbUrl))
            assertThat(secondImageUrl, `is`(item.secondRetinaDisplayThumbUrl))
        }
    }


    @Test
    fun that_presenter_sets_the_selected_item_in_the_model(){

        val item = listingResponse.listingResults?.listings?.get(0)
        presenter.interactions.onItemClicked(item!!)

        verify { mocks.listingsModel setProperty "selectedItem" value item }

    }

    /* a component that injects mocks */
    @Component(modules = [PresenterTestMocks::class])
    interface PresenterTestComponent {
        fun inject(listingPresenter: PropertiesListPresenter)
    }

    /* a module with all the mocks */
    @Module
    class PresenterTestMocks {

        init {
            MockKAnnotations.init(this)

            // setup some mocks here to prettify the test's setup method
            with(listingViewHelper) {
                every { getPriceText(any()) } returns "100,000,000$"
                every { getSpecificationsText(any()) } returns "1000 Rooms, 1 Bath"
                every { getAddressText(any()) } returns "42 Wallaby Way, Sydney"
            }
        }

        @RelaxedMockK
        lateinit var listingsModel: ListingModel

        @RelaxedMockK
        lateinit var listingsViewMediator: PropertiesListViewMediator

        @RelaxedMockK
        lateinit var listingViewHelper: PropertyListingViewHelper


        @Provides
        fun provideListingModel(): ListingModel = listingsModel

        @Provides
        fun provideListingsViewMediator(): PropertiesListViewMediator = listingsViewMediator

        @Provides
        fun providePropertyListingViewHelper(): PropertyListingViewHelper = listingViewHelper
    }
}

