package dg.steiner.ron.com.au.propertylisting.model.objects

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test
import java.io.IOException

/*
Test that the object mapper correctly deserializes a ListingResponse
 */
class ListingResponseDeserialisationTest {

    lateinit var objectMapper: ObjectMapper

    @Before
    fun setup() {

        objectMapper = ObjectMapper()
            .registerModule(KotlinModule())
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

    }

    @Test
    @Throws(IOException::class)
    fun that_object_mapper_reads_listing_json_correctly() {
        val resource = ClassLoader.getSystemResourceAsStream("raw/listing_response.json")
        assertTrue(resource.available() > 0)
        val response = objectMapper.readValue(resource, ListingResponse::class.java)
        assertNotNull(response)
        assertNotNull(response.listingResults)
        assertNotNull(response.listingResults!!.listings)
        assertTrue(response.listingResults!!.listings!!.size == 10)
    }



}