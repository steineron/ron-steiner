package dg.steiner.ron.com.au.propertylisting.presenter

import dagger.Component
import dagger.Module
import dagger.Provides
import dg.steiner.ron.com.au.propertylisting.model.ListingModel
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingItem
import dg.steiner.ron.com.au.propertylisting.model.objects.ListingResponse
import dg.steiner.ron.com.au.propertylisting.view.mediators.SelectedPropertyViewMediator
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject
import org.junit.After
import org.junit.Before
import org.junit.Test
import tests.utils.TestUtils

class SelectedPropertyPresenterTest {
    lateinit var testUtils: TestUtils
    // mocks
    lateinit var mocks: PresenterTestMocks
    // SUT
    lateinit var presenter: SelectedPropertyPresenter

    private lateinit var listingResponse: ListingResponse

    private val itemPublisher: PublishSubject<ListingItem> = spyk(PublishSubject.create())

    @Before
    fun setUp() {
        testUtils = TestUtils()
        testUtils.convertAllSchedulersToTest()
        mocks = PresenterTestMocks()
        presenter = SelectedPropertyPresenter()

        DaggerSelectedPropertyPresenterTest_PresenterTestComponent
            .builder()
            .presenterTestMocks(mocks)
            .build()
            .inject(presenter)

        // mock the publisher
        every { mocks.listingsModel.selectedItemPublisher } returns itemPublisher

        // build a response from captured one - it will have 10 items
        listingResponse = testUtils.objectFromResource(
            "raw/listing_response.json",
            ListingResponse::class.java
        )!!
    }

    @After
    fun tearDown() {
        testUtils.resetSchedulers()
    }


    @Test
    fun that_presenter_subscribes_to_changes_in_selected_item(){
        verify { mocks.listingsModel.selectedItemPublisher  wasNot Called }

        presenter.start()

        verify { itemPublisher.subscribe(any<Consumer<ListingItem>>()) }

    }

    @Test
    fun that_presenter_responds_to_changes_in_selected_item(){
        verify { itemPublisher  wasNot Called }
        verify { mocks.listingsViewMediator  wasNot Called }

        presenter.start()

        val item = listingResponse.listingResults?.listings?.get(0)
       itemPublisher.onNext(item!!)

        testUtils.testScheduler.triggerActions()

        verify { mocks.listingsViewMediator setProperty "itemId" value item.adId}

    }


    /* a component that injects mocks */
    @Component(modules = [PresenterTestMocks::class])
    interface PresenterTestComponent {
        fun inject(presenter: SelectedPropertyPresenter)
    }

    /* a module with all the mocks */
    @Module
    class PresenterTestMocks {

        init {
            MockKAnnotations.init(this)
        }

        @RelaxedMockK
        lateinit var listingsModel: ListingModel

        @RelaxedMockK
        lateinit var listingsViewMediator: SelectedPropertyViewMediator


        @Provides
        fun provideListingModel(): ListingModel = listingsModel

        @Provides
        fun provideListingsViewMediator(): SelectedPropertyViewMediator = listingsViewMediator

    }
}