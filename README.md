# RON-STEINER

This is the implementation of an app for Domain-Group technical assessment.

## To The Reviewer(s)
I'd like to mention a number of things to you as you review this submission

1. The requirements mention "_screen is at above conventional phone width_" and the interpretation I've chosen to follow is landscape mode. This is in accord with Hollie (the recruiter) and after confirming with her.

2. Test coverage and documentation - being aware of time, and aiming to time-box the submission, while still working full-time and caring for family - I've provided a decent - not extensive - tests coverage (being TDD), and documentation through commenting.  


Thank you for reviewing my work.


# High-Level Technical Design

The application is required to display a list of property listings, be able to refresh that list and support orientation changes (that without refreshing from the api endpoint).

The SOLID principles will guide the implementation

## Architectural Pattern

At a high-level glance - the implementation utilises a variant of the classic MV(C/P) architectural patterns.
The variant is close to MVP's Passive View variant, with space for a Controller to be involved too.

The variant defines the following key architectural components:

1. **Model** - encapsulating/abstracting data and data operations.

2. **View (View-Mediator)** - high-level abstraction over an actual Android view.

3. **Controller** - a set of power-classes that control the task's flow. Control means they do not need to adhere to the IoC principle and are rater the representation of control for lower-level classes (Presenter,Model,Mediator etc). Allowing Activities/Fragments/Services to reside in the Controller leaves them with fewer dependencies

4. **Presenter** - a view-aware component that governs presentation of data in the view.

5. **Interactions** (Callbacks) - per-view abstraction of user interactions with the view.

### The Task's Flow
Generally speaking, the flow of data and interactions is:

1. A controller comes to life (e.g. an `Activity`/`Fragment`).

2. The Controller *creates* the required components for the task (e.g. display a list of properties):
    * A View (inflates) and a ViewMediator wrapping that view.
    * A Presenter.
    * (Optional) A Model.
    * (Optional) An implementation of the Interactions.

3. The controller injects each of the components with its dependencies (e.g. the presenter depends on the model and view-mediator, mediator depends on the interactions).

4. The controller sets the task/flow in motion and terminates it when done (e.g. start/stop the presenter from `onResume`/`onPause`).

5. When the task starts:

    * The presenter fetches the required data from the model.
    * The model will fetch data (if it has none or the cache is to be ignored) and "persist" it (for this submission - in-memory caching only).


6. While the task is active:
    * User's interactions with the view are captured by the view mediator and delegated to the interactions-callbacks (e.g. user swiped to refresh, user clicked on an item etc).
    * Interactions can trigger changes in the view, or the model (e.g. set the selected item to view, refresh the data,  etc).
    * The presenter responds to changes in the data, passes view-oriented representation of the data to the view (using the view-mediator).

7. When the task terminates:
    * The presenter unregisters (unsubscribe) from the model.

### Illustrations

(these are illustration for a design document I created a while ago that uses an event-bus, and are very similar to how I approached this exercise)
![instantiations](./tech/assets/instanciations.png "The controller creating the components for the task")


![dependencies](./tech/assets/dependencies.png "Each component's dependencies")


![flow](./tech/assets/flows.png "The flow of data and interactions")


## Choice of Components
* Dagger for dependency-injection. Used a bit loosely here (a very simple app). key points to notice are:
  * how the model is provided as a real singleton to components that include the `ApplicationModule`
  * Fragment and Activity require no injection (which, if possible, I consider a good thing)
* RxJava - for async operations, and as an emulator for an event-bus (using Subject)
* Fresco - for image loading. tho it uses a custom derived version of ImageView (see how I mitigated it with `AppImageView`)
* Retrofit+Jackson for fetching data from the API.
